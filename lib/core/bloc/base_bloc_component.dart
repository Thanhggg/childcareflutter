import 'package:rxdart/subjects.dart';

import '../core.dart';

abstract class BaseBloc {
  Map<String, dynamic> _controllerMap;
  PublishSubject<FlutEventModel> _eventHandlePoint;
  StreamSubscription _eventHandleDisposable;
  BaseBloc()
      : _eventHandlePoint = PublishSubject(),
        _controllerMap = Map() {
    _eventHandleDisposable = _eventHandlePoint
        .where((event) => event is FlutEventModel)
        .listen((event) {
      onReceivedEvent(event.getMessage, event.getData);
    });
  }

  LiveData getLiveData(String name) {
    if (_controllerMap.containsKey(name)) {
      return _controllerMap[name];
    }

    var liveData = LiveData();
    _controllerMap[name] = liveData;
    return liveData;
  }

  StreamController getStreamController<T>(String name) {
    if (_controllerMap.containsKey(name)) {
      return _controllerMap[name];
    }

    var streamController = StreamController<T>.broadcast();
    _controllerMap[name] = streamController;
    return streamController;
  }

  void showLoading() {
    FlutBus.pushEvent(CoreEvent.SHOW_LOADING, true);
    getStreamController('loading').add(true);
  }

  void hideLoading() {
    FlutBus.pushEvent(CoreEvent.SHOW_LOADING, false);
    getStreamController('loading').add(false);
  }

  void unRegisterController() {
    if (_controllerMap.isNotEmpty) {
      for (var controller in _controllerMap.values) {
        try {
          controller.close();
        } catch (e, s) {
          print(s);
        }
      }
      _controllerMap.clear();
    }
  }

  void dispose() {
    _eventHandleDisposable.cancel();
    unRegisterController();
  }

  void onReceivedEvent(String message, dynamic data) {}

  void onBlocReady() {}
}
