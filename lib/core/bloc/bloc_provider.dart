import 'package:flutter/widgets.dart';
import '../bloc/base_bloc_component.dart';

class BlocProvider<T extends BaseBloc> extends StatefulWidget {
  final T bloc;
  final Widget widget;

  BlocProvider({Key key, @required this.bloc, @required this.widget})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return BlocProviderState<T>();
  }
}

class BlocProviderState<T extends BaseBloc> extends State<BlocProvider<T>> {
  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseBlocProvider(
      key: widget.key,
      bloc: widget.bloc,
      widget: widget.widget,
    );
  }
}

class BaseBlocProvider<T extends BaseBloc> extends InheritedWidget {
  final T bloc;
  final Widget widget;

  BaseBlocProvider({Key key, @required this.bloc, @required this.widget})
      : super(key: key, child: widget);
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }
}

class BlocProviders {

  static T of<T extends BaseBloc>(BuildContext context) {
    BaseBlocProvider provider =
        context.dependOnInheritedWidgetOfExactType<BaseBlocProvider<T>>();
    return provider.bloc;
  }
}
