import 'package:flutter/material.dart';

abstract class BaseApp extends StatefulWidget {
  final String _title;

  BaseApp(this._title);

  void onInitFeature(BuildContext context) {}
  void onAppDispose() {}
  Widget rootBuilder(BuildContext context);
  ThemeData setupTheme() {
    return ThemeData.light();
  }

  @override
  State<StatefulWidget> createState() {
    return BaseAppState();
  }
}

class BaseAppState extends State<BaseApp> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: widget._title,
      theme: widget.setupTheme(),
      home: Builder(
        builder: (context) {
          widget.onInitFeature(context);
          return widget.rootBuilder(context);
        },
      ),
    );
  }

  @override
  void dispose() {
    print("BaseApp dipose");
    widget.onAppDispose();
    super.dispose();
  }
}
