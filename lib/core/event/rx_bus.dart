import 'dart:async';

import 'package:rxdart/rxdart.dart';

import 'rx_event_model.dart';

class FlutBus {
  static PublishSubject<dynamic> _publishSubject;
  static List<StreamSubscription> _subScriptionList;

  static PublishSubject get eventSource => _publishSubject;

  static void init() {
    _publishSubject = PublishSubject();
    _subScriptionList = List();
  }

  static StreamSubscription<T> listen<T>(
      String event, OnDataReceive<T> onDataReceive) {
    StreamSubscription subscription = _publishSubject
        .where((data) =>
            data is FlutEventModel &&
            data.getMessage == event &&
            data.getData != null &&
            data.getData is T)
        .map((data) => ((data as FlutEventModel).getData as T))
        .listen((onData) => onDataReceive(onData));
    _subScriptionList.add(subscription);
    return subscription;
  }

  static StreamSubscription<T> listenDistinct<T>(
      String event, OnDataReceive<T> onDataReceive) {
    StreamSubscription subscription = _publishSubject
        .where((data) =>
            data is FlutEventModel &&
            data.getMessage == event &&
            data.getData != null &&
            data.getData is T)
        .distinct((oldMsg, newMsg) =>
            (oldMsg as FlutEventModel).getMessage ==
            (newMsg as FlutEventModel).getMessage)
        .map((data) => ((data as FlutEventModel).getData as T))
        .listen((onData) => onDataReceive(onData));
    _subScriptionList.add(subscription);
    return subscription;
  }

  static void pushEvent(String event, dynamic data,
      {Duration delayed = const Duration(seconds: 0)}) {
    Timer(delayed, () {
      _publishSubject.add(FlutEventModel(event, data));
    });
  }

  static void unSubscrible([StreamSubscription subscription]) {
    if (subscription != null) {
      subscription.cancel();
    }
    if (_subScriptionList != null && _subScriptionList.contains(subscription)) {
      _subScriptionList.remove(subscription);
    }
  }

  static void unSubscribleAll() {
    if (_subScriptionList != null && _subScriptionList.isNotEmpty) {
      for (var subscription in _subScriptionList) {
        if (subscription != null) subscription.cancel();
      }
      _subScriptionList.clear();
    }
    _publishSubject.close();
  }
}

typedef OnDataReceive<T> = FutureOr<void> Function(T params);
