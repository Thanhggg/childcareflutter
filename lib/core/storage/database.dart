import 'package:hello_world/core/core.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  static Database _database;

  static void init(String dbName,
      {FutureOr<void> Function(Database db, int version) onCreate,
      FutureOr<void> Function(Database db, int oldVer, int newVer) onUpgrade,
      int version = 1}) async {
    _database = await openDatabase(join(await getDatabasesPath(), dbName),
        onCreate: onCreate, onUpgrade: onUpgrade, version: version);
  }

  static Database getDatabase() {
    return _database;
  }
}
