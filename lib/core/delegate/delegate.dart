typedef void Action1<T>(T arg);
typedef void Action2<T, K>(T arg1, K arg2);
typedef void Action3<T, K, L>(T arg1, K arg2, L arg3);
typedef void Action4<T, K, L, M>(T arg1, K arg2, L arg3, M arg4);

typedef R Function1<R, T>(T arg);
typedef R Function2<R, T, T1>(T arg, T1 arg1);
typedef R Function3<R, T, T1, T2>(T arg, T1 arg1, T2 arg2);
typedef R Function4<R, T, T1, T2, T3>(T arg, T1 arg1, T2 arg2, T3 arg3);
