import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

abstract class DioService {
  DioService init({
    BaseOptions baseOptions,
    List<Interceptor> interceptors,
    String authenticationSchema,
    OnTokenGet getTokenMethod,
  });
  void cancelRequests();

  Future<void> get<T>(
    String path, {
    Map<String, dynamic> queryParameters,
    Options options,
    bool cancelToken,
    ProgressCallback onReceiveProgress,
    @required OnRespond onRespond,
    @required OnError onError,
  });

  Future<void> post<T>(
    String path, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
    @required OnRespond onRespond,
    @required OnError onError,
  });

  Future<void> put<T>(
    String path, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
    @required OnRespond onRespond,
    @required OnError onError,
  });

  Future<void> delete<T>(
    String path, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    @required OnRespond onRespond,
    @required OnError onError,
  });

  Future<Response<T>> request<T>(
    String path, {
    data,
    Map<String, dynamic> queryParameters,
    CancelToken cancelToken,
    Options options,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  });

  void lock();
  void unlock();

  Future<Response<T>> reject<T>(dynamic error);
}

typedef OnRespond = void Function(Response respond);
typedef OnError = void Function(DioError error);
typedef OnTokenGet = Future<String> Function();
