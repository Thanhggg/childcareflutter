import 'package:dio/dio.dart';
import 'package:hello_world/core/network2/interface.dart';

class DioNonAuthService implements DioService {
  static final DioService _instance = DioNonAuthService._internal();

  DioNonAuthService._internal() {
    _client = Dio();
    _cancelToken = CancelToken();
  }
  factory DioNonAuthService() => _instance;

  Dio _client;
  CancelToken _cancelToken;

  @override
  DioService init(
      {BaseOptions baseOptions,
      List<Interceptor> interceptors,
      String authenticationSchema,
      OnTokenGet getTokenMethod}) {
    _client.options = baseOptions;
    if (interceptors != null && interceptors.isNotEmpty) {
      _client.interceptors.clear();
      _client.interceptors.addAll(interceptors);
    }

    return _instance;
  }

  @override
  void cancelRequests() {
    _cancelToken?.cancel('cancel');
  }

  @override
  Future<void> get<T>(String path,
      {Map<String, dynamic> queryParameters,
      Options options,
      bool cancelToken,
      onReceiveProgress,
      onRespond,
      onError}) async {
    try {
      var respond = await _client.get(path,
          queryParameters: queryParameters,
          options: options,
          onReceiveProgress: onReceiveProgress,
          cancelToken: cancelToken ?? _cancelToken);

      if (onRespond != null) onRespond(respond);
    } on DioError catch (e) {
      if (onError != null) onError(e);
    }
  }

  @override
  Future<void> post<T>(String path,
      {data,
      Map<String, dynamic> queryParameters,
      Options options,
      CancelToken cancelToken,
      onSendProgress,
      onReceiveProgress,
      onRespond,
      onError}) async {
    try {
      var respond = await _client.post(path,
          queryParameters: queryParameters,
          options: options,
          onReceiveProgress: onReceiveProgress,
          onSendProgress: onSendProgress,
          data: data,
          cancelToken: cancelToken ?? _cancelToken);
      if (onRespond != null) onRespond(respond);
    } on DioError catch (e) {
      if (onError != null) onError(e);
    }
  }

  @override
  Future<void> put<T>(String path,
      {data,
      Map<String, dynamic> queryParameters,
      Options options,
      CancelToken cancelToken,
      onSendProgress,
      onReceiveProgress,
      onRespond,
      onError}) async {
    try {
      var respond = await _client.put(path,
          queryParameters: queryParameters,
          options: options,
          onReceiveProgress: onReceiveProgress,
          onSendProgress: onSendProgress,
          data: data,
          cancelToken: cancelToken ?? _cancelToken);
      if (onRespond != null) onRespond(respond);
    } on DioError catch (e) {
      if (onError != null) onError(e);
    }
  }

  @override
  Future<void> delete<T>(String path,
      {data,
      Map<String, dynamic> queryParameters,
      Options options,
      CancelToken cancelToken,
      onRespond,
      onError}) async {
    try {
      var respond = await _client.delete(path,
          queryParameters: queryParameters,
          options: options,
          data: data,
          cancelToken: cancelToken ?? _cancelToken);
      if (onRespond != null) onRespond(respond);
    } on DioError catch (e) {
      if (onError != null) onError(e);
    }
  }

  @override
  void lock() {
    _client.interceptors.requestLock.lock();
    _client.interceptors.responseLock.lock();
  }

  @override
  Future<Response<T>> request<T>(String path,
      {data,
      Map<String, dynamic> queryParameters,
      CancelToken cancelToken,
      Options options,
      onSendProgress,
      onReceiveProgress}) {
    return _client.request(path,
        data: data,
        queryParameters: queryParameters,
        cancelToken: cancelToken ?? _cancelToken,
        options: options,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress);
  }

  @override
  Future<Response<T>> reject<T>(dynamic error) {
    return _client.reject(error);
  }

  @override
  void unlock() {
    _client.interceptors.requestLock.unlock();
    _client.interceptors.responseLock.unlock();
  }
}
