import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:hello_world/core/network2/interface.dart';

class DioAuthService implements DioService {
  static final DioAuthService _instance = DioAuthService._internal();

  Dio _client;
  CancelToken _cancelToken;
  String _authenticationSchema;
  OnTokenGet _getTokenMethod;

  DioAuthService._internal() {
    this._client = Dio();
    _cancelToken = CancelToken();
  }
  factory DioAuthService() => _instance;

  @override
  DioService init({
    BaseOptions baseOptions,
    List<Interceptor> interceptors,
    @required String authenticationSchema,
    @required OnTokenGet getTokenMethod,
  }) {
    _authenticationSchema = authenticationSchema;
    _getTokenMethod = getTokenMethod;
    _client.options = baseOptions;
    _client.interceptors.clear();
    if (interceptors != null && interceptors.isNotEmpty)
      _client.interceptors.addAll(interceptors);

    return _instance;
  }

  @override
  Future<void> get<T>(String path,
      {Map<String, dynamic> queryParameters,
      Options options,
      bool cancelToken,
      ProgressCallback onReceiveProgress,
      OnRespond onRespond,
      OnError onError}) async {
    try {
      var authOption = options ?? Options();
      authOption.headers[HttpHeaders.authorizationHeader] =
          '$_authenticationSchema ${await _getTokenMethod()}';
      var respond = await _client.get(path,
          queryParameters: queryParameters,
          options: options ?? authOption,
          onReceiveProgress: onReceiveProgress,
          cancelToken: cancelToken ?? _cancelToken);

      if (onRespond != null) onRespond(respond);
    } on DioError catch (e) {
      if (onError != null) onError(e);
    }
  }

  @override
  Future<void> post<T>(String path,
      {data,
      Map<String, dynamic> queryParameters,
      Options options,
      CancelToken cancelToken,
      ProgressCallback onSendProgress,
      ProgressCallback onReceiveProgress,
      OnRespond onRespond,
      OnError onError}) async {
    try {
      var authOption = options ?? Options();
      authOption.headers[HttpHeaders.authorizationHeader] =
          '$_authenticationSchema ${await _getTokenMethod()}';
      var respond = await _client.post(path,
          queryParameters: queryParameters,
          options: authOption,
          onReceiveProgress: onReceiveProgress,
          onSendProgress: onSendProgress,
          data: data,
          cancelToken: cancelToken ?? _cancelToken);
      if (onRespond != null) onRespond(respond);
    } on DioError catch (e) {
      if (onError != null) onError(e);
    }
  }

  @override
  Future<void> put<T>(String path,
      {data,
      Map<String, dynamic> queryParameters,
      Options options,
      CancelToken cancelToken,
      ProgressCallback onSendProgress,
      ProgressCallback onReceiveProgress,
      OnRespond onRespond,
      OnError onError}) async {
    try {
      var authOption = options ?? Options();
      authOption.headers[HttpHeaders.authorizationHeader] =
          '$_authenticationSchema ${await _getTokenMethod()}';
      var respond = await _client.put(path,
          queryParameters: queryParameters,
          options: authOption,
          onReceiveProgress: onReceiveProgress,
          onSendProgress: onSendProgress,
          data: data,
          cancelToken: cancelToken ?? _cancelToken);
      if (onRespond != null) onRespond(respond);
    } on DioError catch (e) {
      if (onError != null) onError(e);
    }
  }

  @override
  Future<void> delete<T>(String path,
      {data,
      Map<String, dynamic> queryParameters,
      Options options,
      CancelToken cancelToken,
      OnRespond onRespond,
      OnError onError}) async {
    try {
      var authOption = options ?? Options();
      authOption.headers[HttpHeaders.authorizationHeader] =
          '$_authenticationSchema ${await _getTokenMethod()}';
      var respond = await _client.delete(path,
          queryParameters: queryParameters,
          options: authOption,
          data: data,
          cancelToken: cancelToken ?? _cancelToken);
      if (onRespond != null) onRespond(respond);
    } on DioError catch (e) {
      if (onError != null) onError(e);
    }
  }

  @override
  void cancelRequests() {
    _cancelToken.cancel('cancel');
  }

  @override
  void lock() {
    _client.lock();
    _client.interceptors.responseLock.lock();
    _client.interceptors.errorLock.lock();
  }

  @override
  Future<Response<T>> reject<T>(dynamic error) {
    return _client.reject(error);
  }

  @override
  Future<Response<T>> request<T>(
    String path, {
    data,
    Map<String, dynamic> queryParameters,
    CancelToken cancelToken,
    Options options,
    onSendProgress,
    onReceiveProgress,
  }) {
    return _client.request(path,
        data: data,
        queryParameters: queryParameters,
        cancelToken: cancelToken ?? _cancelToken,
        options: options,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress);
  }

  @override
  void unlock() {
    _client.unlock();
    _client.interceptors.responseLock.unlock();
    _client.interceptors.errorLock.unlock();
  }
}
