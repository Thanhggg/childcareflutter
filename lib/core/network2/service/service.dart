import 'package:flutter/cupertino.dart';
import 'package:hello_world/core/network2/interface.dart';

class HttpServices {
  static DioService authServices;
  static DioService unAuthServices;

  static void init({@required authenService, unAuthenService}) {
    authServices = authenService;
    unAuthServices = unAuthenService;
  }

  static void cancelRequests() {
    authServices?.cancelRequests();
    unAuthServices?.cancelRequests();
  }
}
