import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:hello_world/core/core.dart';
import 'package:hello_world/core/network2/interface.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenIntercepter extends Interceptor {
  static final errorQueue = List<Completer>();
  final DioService dio;
  final SharedPreferences tokenStorage;
  TokenIntercepter({@required this.dio, @required this.tokenStorage});

  @override
  Future onRequest(RequestOptions options) async {
    return options;
  }

  @override
  Future onResponse(Response response) async {
    return response;
  }

  @override
  Future onError(DioError error) async {
    if (error.response?.statusCode == 401) {
      String oldToken = error.request.headers[HttpHeaders.authorizationHeader];
      print(
          'HttpService: need refresh token ---- \x1b[34m ${error.request.uri}\t \x1b[31m waiting ----------');
      var newOption = error.request;
      String newToken = 'bearer ${tokenStorage.getString('TOKEN')}';
      if (oldToken == newToken) {
        if (TokenRefreshStatus.isRefreshing) {
          var completer = Completer();
          errorQueue.add(completer);
          await completer.future;
          newToken = 'bearer ${tokenStorage.getString('TOKEN')}';
          print(
              'HttpService:\x1b[34m ${error.request.uri}\t \x1b[32m restart in new token ------');
          newOption.headers[HttpHeaders.authorizationHeader] = newToken;
        } else {
          TokenRefreshStatus.isRefreshing = true;
          var refreshTokenRespond = await refreshToken(
              tokenStorage.getString('REFRESH_TOKEN'),
              tokenStorage.getString('EMAIL'));

          if (refreshTokenRespond == null ||
              refreshTokenRespond.statusCode != 200 ||
              refreshTokenRespond.data['refreshToken'] == null ||
              refreshTokenRespond.data['token'] == null) {
            FlutBus.pushEvent('LOG_OUT', true);
            print(
                'HttpService: \x1b[33m ------------------------------------ refresh token failed ---- logout');
            return dio.reject(DioError(
                request: error.request,
                response: error.response,
                type: DioErrorType.CANCEL,
                error: 'can not refresh token'));
          } else {
            print(
                'HttpService:\x1b[33m refresh token done: ${refreshTokenRespond.data['token']} ------');
            await tokenStorage.setString(
                'REFRESH_TOKEN', refreshTokenRespond.data['refreshToken']);
            await tokenStorage.setString(
                'TOKEN', refreshTokenRespond.data['token']);
            newOption.headers[HttpHeaders.authorizationHeader] =
                "bearer ${refreshTokenRespond.data["token"]}";
            print(
                'HttpService:\x1b[34m ${error.request.uri}\t \x1b[32m restart in refresh token ------');
          }
          TokenRefreshStatus.isRefreshing = false;
          for (var item in errorQueue) {
            item.complete();
          }
          errorQueue.clear();
        }
      } else {
        print(
            'HttpService:\x1b[34m ${error.request.uri}\t \x1b[32m restart in new token ------');
        newOption.headers[HttpHeaders.authorizationHeader] = newToken;
      }
      return dio.request(error.request.path, options: newOption);
    } else {
      return error;
    }
  }
}

Future<Response> refreshToken(String refreshToken, String email) async {
  try {
    print(
        'HttpService:\x1b[33m ------------------------------- refresh token: $refreshToken -----------------------------------------------');
    var dio = Dio(BaseOptions(
      baseUrl: 'https://nestapisd.herokuapp.com/',
      responseType: ResponseType.json,
    ));
    var respond = await dio.post('/user/refreshToken',
        data: jsonEncode({
          "refreshToken": refreshToken,
          "email": email,
        }));
    return respond;
  } on DioError {
    return null;
  }
}

class TokenRefreshStatus {
  static bool isRefreshing = false;
}
