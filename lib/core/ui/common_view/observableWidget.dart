import '../../../core/event/observable.dart';
import 'package:flutter/material.dart';

class ObservableWidget<T> extends StatelessWidget {
  final ObservableField<T> observable;
  final OnBuild<T> onbuild;
  final OnErrorBuild onErrorBuild;
  ObservableWidget(
      {Key key,
      @required this.observable,
      @required this.onbuild,
      this.onErrorBuild})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<T>(
      initialData: observable.value(),
      stream: observable.get(),
      builder: (BuildContext context, AsyncSnapshot<T> data) {
        if (data.hasData)
          return onbuild(context, data.data);
        else if (onErrorBuild != null)
          return onErrorBuild(context, data.error);
        else
          return Container(
            width: 0.0,
            height: 0.0,
          );
      },
    );
  }
}

typedef Widget OnBuild<T>(BuildContext context, T value);
typedef Widget OnErrorBuild(BuildContext context, dynamic error);
