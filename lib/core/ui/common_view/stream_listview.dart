import 'package:flutter/material.dart';
import '../../core.dart';

class StreamListView<T> extends StatelessWidget {
  final Stream<List<T>> listItemStream;
  final OnItemBuilder<T> itemBuilder;
  final IndexedWidgetBuilder headerBuilder;
  final Axis scrollDirection;
  final int numberItemInRow;

  StreamListView(
      {@required this.listItemStream,
      @required this.itemBuilder,
      this.scrollDirection,
      this.headerBuilder,
      this.numberItemInRow = 1});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<T>>(
      initialData: [],
      stream: this.listItemStream,
      builder: (context, data) => data == null
          ? Center()
          : this.numberItemInRow <= 1
              ? this.headerBuilder == null
                  ? ListView.builder(
                      itemBuilder: (context, position) =>
                          itemBuilder(context, position, data.data[position]),
                      itemCount: data.data.length,
                    )
                  : ListView.separated(
                      itemBuilder: (context, position) =>
                          itemBuilder(context, position, data.data[position]),
                      separatorBuilder: headerBuilder,
                      itemCount: data.data.length,
                    )
              : GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: this.numberItemInRow),
                  itemCount: data.data.length,
                  itemBuilder: (context, position) =>
                      itemBuilder(context, position, data.data[position]),
                ),
    );
  }
}

typedef OnItemBuilder<T> = Widget Function(
    BuildContext context, int position, T item);
