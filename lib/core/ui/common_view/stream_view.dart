import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../../core/util/string_util.dart';

class StreamTextField extends StatelessWidget {
  final Key key;
  final bool isPassword;
  final TextInputType inputType;
  final TextEditingController textController;
  final int maxLines;
  final int maxLength;
  final TextStyle style;
  final TextAlign align;
  final String hint;
  final String label;
  final InputBorder border;
  final InputBorder errorBorder;
  final StreamController<dynamic> streamController;
  final ValueChanged<String> onChange;
  final EdgeInsetsGeometry padding;
  final isShowError;

  StreamTextField({
    this.isShowError = true,
    this.textController,
    this.key,
    this.isPassword = false,
    this.inputType = TextInputType.text,
    this.maxLines = 1,
    this.maxLength = 255,
    this.style = const TextStyle(fontSize: 14, color: Colors.black),
    this.align = TextAlign.left,
    this.streamController,
    this.onChange,
    this.hint,
    this.label,
    this.border = const UnderlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        borderSide: BorderSide(
          style: BorderStyle.solid,
          width: 1.0,
        )),
    this.errorBorder = const UnderlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        borderSide: BorderSide(
          style: BorderStyle.solid,
          width: 1.0,
        )),
    this.padding =
        const EdgeInsets.only(left: 5, top: 10, bottom: 10, right: 10),
  }) : super(key: key) {
    if (this.textController != null) {
      this.textController.addListener(() {
        streamController?.add(this.textController.text);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      initialData: '',
      stream: this.streamController.stream,
      builder: (context, snapshot) => TextField(
            controller: this.textController,
            maxLines: this.maxLines,
            cursorColor: Colors.blue[600],
            obscureText: isPassword,
            keyboardType: this.inputType,
            style: this.style,
            textAlign: TextAlign.left,
            textInputAction: TextInputAction.done,
            onChanged: (text) {
              if (this.textController == null) {
                this.streamController?.sink?.add(text);
                if (this.onChange != null) this.onChange(text);
              }
            },
            inputFormatters: [
              LengthLimitingTextInputFormatter(this.maxLength),
            ],
            decoration: InputDecoration(
                labelText: this.label,
                hintText: this.hint,
                hintStyle: TextStyle(fontSize: 14, color: Colors.grey),
                labelStyle: TextStyle(fontSize: 20),
                errorText: snapshot != null &&
                        !StringUtil.isEmptyTrim(snapshot.error) &&
                        isShowError
                    ? snapshot.error
                    : null,
                errorBorder: this.errorBorder,
                border: this.border,
                contentPadding: this.padding),
          ),
    );
  }
}
