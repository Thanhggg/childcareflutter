import 'package:flutter/material.dart';
import '../../delegate/delegate.dart';

class CounterView extends StatelessWidget {
  int initNumber;
  final Action1<int> onCouterChanged;

  CounterView(this.initNumber, {this.onCouterChanged}) {
    if (this.initNumber < 0) this.initNumber = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            if (initNumber > 0) initNumber--;
            if (onCouterChanged != null) onCouterChanged(initNumber);
          },
          child: Container(
            color: Colors.blue,
            child: Icon(Icons.remove, color: Colors.black),
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          child: Text('$initNumber'),
        ),
        GestureDetector(
          onTap: () {
            initNumber++;
            if (onCouterChanged != null) onCouterChanged(initNumber);
          },
          child: Container(
            color: Colors.blue,
            child: Icon(Icons.add, color: Colors.black),
          ),
        ),
      ],
    );
  }
}
