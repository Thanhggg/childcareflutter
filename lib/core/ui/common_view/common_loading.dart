import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Center(
        child: Image.asset(
          'assets/images/loading.gif',
          width: 80.0,
          height: 80.0,
        ),
      ),
    );
  }
}
