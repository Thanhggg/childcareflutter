import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';

import '../../../core/core.dart';

abstract class BasePageController extends StatefulWidget {
  final Key key;
  BasePageController({this.key}) : super(key: key);

  String getPageName() => this.toStringShort();
}

abstract class BasePage<T extends BaseBloc> extends BasePageController {
  final BasePageProperties _properties = BasePageProperties();

  BasePage({Key key, NavigatorState navigator}) : super(key: key) {
    _properties.navigator = navigator;
    _properties.pageKey = key;
  }

  @override
  State<StatefulWidget> createState() {
    return BasePageState();
  }

  Widget onPageBuild(BuildContext context);

  void onPrepareBuild(BuildContext context) {
    _properties.context = context;
  }

  void onInitEvent(BuildContext context) {
    bloc.getStreamController('loading').stream.listen((isLoading) {
      showLoadingDialog(context, isLoading);
    });
  }

  void onInitBlocData(BuildContext context) {
    if (_properties.bloc == null) _properties.bloc = getBlocData(context);
  }

  T getBlocData(BuildContext context);

  T get bloc => this._properties.bloc;

  void clearFocus(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
  }

  GlobalKey<BasePageState> get pageKey => _properties.pageKey;

  void onDispose() {
    BackButtonInterceptor.remove(onBackBtnPress);
  }

  void onPageInit() {
    BackButtonInterceptor.add(onBackBtnPress);
  }

  NavigatorState getNavigator(BuildContext context) {
    if (this._properties.navigator == null)
      return Navigator.of(context);
    else
      return this._properties.navigator;
  }

  bool onBackBtnPress(bool isDisableDefaultBack) {
    return onBackPress(_properties.context);
  }

  bool onBackPress(BuildContext context) {
    return false;
  }

  void showErrorDialog(BuildContext context, String msg) async {
    await Future.delayed(Duration(milliseconds: 200));
    showDialog(
        useRootNavigator: false,
        context: context,
        child: AlertDialog(
          title: Text("Oops"),
          content: Text(msg),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"))
          ],
        ));
  }

  void showLoadingDialog(BuildContext context, bool isShow) {
    if (_properties.isLoading != isShow) {
      _properties.isLoading = isShow;
      if (isShow)
        showDialog(
          useRootNavigator: false,
          context: context,
          builder: (context) => Container(
            color: Colors.transparent,
            child: Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.grey,
              ),
            ),
          ),
        );
      else
        Navigator.pop(context);
    }
  }
}
