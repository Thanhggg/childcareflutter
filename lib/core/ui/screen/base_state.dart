import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../../core.dart';

class BasePageState extends State<BasePage> with TickerProviderStateMixin {
  BasePageState() : super();

  StreamSubscription _initEventSubscription;
  final Subject _initEventSubject = PublishSubject();
  bool isInit = false;
  @override
  void initState() {
    onStateInit();
    _initEventSubscription = _initEventSubject
        .where((data) => data is BuildContext)
        .distinct((oldData, newData) => oldData == newData)
        .listen((context) {
      onInitEvent(context);
      _initEventSubscription.cancel();
      return;
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    initBlocProvider(context);
    _initEventSubject.add(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return onPageBuild(context);
  }

  @override
  void dispose() {
    onStateDispose();
    _initEventSubscription.cancel();
    super.dispose();
  }

  void initBlocProvider(BuildContext context) {
    widget.onInitBlocData(context);
  }

  void onInitEvent(BuildContext context) {
    widget.onInitEvent(context);
  }

  void onStateInit() {
    widget.onPageInit();
  }

  Widget onPageBuild(BuildContext context) {
    widget.onPrepareBuild(context);
    return widget.onPageBuild(context);
  }

  void onStateDispose() {
    widget.onDispose();
  }
}

class BasePageProperties<T extends BaseBloc> {
  T bloc;
  NavigatorState navigator;
  Key pageKey;
  bool isLoading;
  BuildContext context;
}
