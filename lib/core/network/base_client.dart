import 'dart:convert';

import '../../core/network/interface/http_service.dart';
import '../../core/network/interface/intercepter.dart';
import '../../core/util/string_util.dart';

abstract class BaseClient implements HttpService {
  static const int DEFAULT_REQUEST_TIMEOUT = 30000;
  static const int TIMEOUT_ERROR_CODE = 1001;
  static const int DEFAULT_ERROR_CODE = 1002;
  static const int REFRESH_TOKEN_FAILED = 1003;
  static const int SUCCESS_CODE = 200;
  String baseUrl;
  int requestTimeout;
  Map<String, String> authenticator;
  List<Intercepter> intercepters;
  int retryCount;
  bool isRetry;

  @override
  get(String url,
      {Map<String, String> header,
      Map<String, String> queries,
      List<String> path,
      OnComplete onComplete,
      OnFailed onFailed});

  @override
  post(String url,
      {Map<String, String> header,
      Encoding encoding,
      body,
      OnComplete onComplete,
      OnFailed onFailed});

  @override
  put(String url,
      {Map<String, String> header,
      Encoding encoding,
      body,
      OnComplete onComplete,
      OnFailed onFailed});

  @override
  delete(String url,
      {Map<String, String> header,
      Map<String, String> queries,
      List<String> path,
      OnComplete onComplete,
      OnFailed onFailed});

  String createUrl(String url,
      {List<String> path, Map<String, String> queries}) {
    String result = StringUtil.isEmptyTrim(baseUrl) ||
            (!StringUtil.isEmptyTrim(url) &&
                (url.contains('http') || url.contains('https')))
        ? ''
        : baseUrl;
        
    if (!StringUtil.isEmptyTrim(url)) {
      result += '$url';
    }
    if (path != null && path.isNotEmpty) {
      for (var item in path) {
        result += '/$item';
      }
    }

    if (queries != null && queries.isNotEmpty) {
      result += '?';
      for (int i = 0; i <= queries.keys.length - 1; i++) {
        result +=
            '${queries.keys.elementAt(i)}=${queries[queries.keys.elementAt(i)] ?? ''}';

        if (i < queries.keys.length - 1) {
          result += '&';
        }
      }
    }

    return result;
  }
}
