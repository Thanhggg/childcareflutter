import '../../core/network/http_client.dart';

class NetWorkRequest {
  static final _singleton = NetWorkRequest.singleton();
  final HttpClient _httpClient = HttpClient.builder
      .setAuthenticator({})
      .setBaseUrl('https://jsonplaceholder.typicode.com')
      .setRequestTimeout(20000)
      .setRetryCount(2)
      .build();
  NetWorkRequest.singleton();

  factory NetWorkRequest() {
    return _singleton;
  }

  HttpClient httpClient() {
    return _httpClient;
  }
}
