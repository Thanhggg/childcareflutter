import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as Client;
import 'package:http/http.dart' show Response;

import '../../core/model/respond_model.dart';
import '../../core/network/base_client.dart';
import '../../core/network/interface/client_builder.dart';
import '../../core/network/interface/http_service.dart';
import '../../core/network/interface/intercepter.dart';

class HttpClient extends BaseClient {
  static final HttpClientBuilder builder = HttpClientBuilder();

  HttpClient(HttpClientBuilder builder) : super() {
    baseUrl = builder.baseUrl;
    requestTimeout = builder.requestTimeout;
    authenticator = builder.authenticator;
    intercepters = builder.intercepters;
    retryCount = builder.retryCount;
  }

  @override
  get(String url,
      {Map<String, String> header,
      Map<String, String> queries,
      List path,
      OnComplete onComplete,
      OnFailed onFailed,
      int counter}) async {
    String completeUrl = createUrl(url, path: path, queries: queries);
    try {
      print('$completeUrl');
      if (counter == null) counter = retryCount;
      Response respond = await Client.get(completeUrl,
              headers: header ?? authenticator ?? null)
          .timeout(Duration(
              milliseconds:
                  requestTimeout ?? BaseClient.DEFAULT_REQUEST_TIMEOUT));
      validateRespond(respond, () {
        get(url,
            header: header,
            queries: queries,
            path: path,
            onComplete: onComplete,
            onFailed: onFailed,
            counter: --counter);
      }, onComplete, onFailed, --counter);
    } on TimeoutException catch (_) {
      if (onFailed != null) {
        onFailed(RespondModel(BaseClient.TIMEOUT_ERROR_CODE, null,
            message: 'request timeout!'));
      }
    } catch (e) {
      onFailed(
          RespondModel(BaseClient.DEFAULT_ERROR_CODE, null, message: 'error!'));
    }
  }

  @override
  post(String url,
      {Map<String, String> header,
      Encoding encoding,
      body,
      OnComplete onComplete,
      OnFailed onFailed,
      counter}) async {
    String completeUrl = createUrl(url);
    try {
      if (counter == null) counter = retryCount;
      Response respond = await Client.post(completeUrl,
              headers: header ?? authenticator ?? null,
              encoding: encoding,
              body: body)
          .timeout(Duration(
              milliseconds:
                  requestTimeout ?? BaseClient.DEFAULT_REQUEST_TIMEOUT));
      validateRespond(respond, () {
        post(url,
            header: header,
            encoding: encoding,
            body: body,
            onComplete: onComplete,
            onFailed: onFailed,
            counter: --counter);
      }, onComplete, onFailed, --counter);
    } on TimeoutException catch (_) {
      if (onFailed != null) {
        onFailed(RespondModel(BaseClient.TIMEOUT_ERROR_CODE, null,
            message: 'request timeout!'));
      }
    } catch (e) {
      onFailed(
          RespondModel(BaseClient.DEFAULT_ERROR_CODE, null, message: 'error!'));
    }
  }

  @override
  put(String url,
      {Map<String, String> header,
      Encoding encoding,
      body,
      OnComplete onComplete,
      OnFailed onFailed,
      counter}) async {
    String completeUrl = createUrl(url);
    try {
      if (counter == null) counter = retryCount;
      Response respond = await Client.post(completeUrl,
              headers: header ?? authenticator ?? null,
              encoding: encoding,
              body: body)
          .timeout(Duration(
              milliseconds:
                  requestTimeout ?? BaseClient.DEFAULT_REQUEST_TIMEOUT));
      validateRespond(respond, () {
        post(url,
            header: header,
            encoding: encoding,
            body: body,
            onComplete: onComplete,
            onFailed: onFailed,
            counter: --counter);
      }, onComplete, onFailed, --counter);
    } on TimeoutException catch (_) {
      if (onFailed != null) {
        onFailed(RespondModel(BaseClient.TIMEOUT_ERROR_CODE, null,
            message: 'request timeout!'));
      }
    } catch (e) {
      onFailed(
          RespondModel(BaseClient.DEFAULT_ERROR_CODE, null, message: 'error!'));
    }
  }

  @override
  delete(String url,
      {Map<String, String> header,
      Map<String, String> queries,
      List path,
      OnComplete onComplete,
      OnFailed onFailed,
      counter}) async {
    String completeUrl = createUrl(url, path: path, queries: queries);
    try {
      print('$completeUrl');
      if (counter == null) counter = retryCount;
      Response respond = await Client.get(completeUrl,
              headers: header ?? authenticator ?? null)
          .timeout(Duration(
              milliseconds:
                  requestTimeout ?? BaseClient.DEFAULT_REQUEST_TIMEOUT));
      validateRespond(respond, () {
        get(url,
            header: header,
            queries: queries,
            path: path,
            onComplete: onComplete,
            onFailed: onFailed,
            counter: --counter);
      }, onComplete, onFailed, --counter);
    } on TimeoutException catch (_) {
      if (onFailed != null) {
        onFailed(RespondModel(BaseClient.TIMEOUT_ERROR_CODE, null,
            message: 'request timeout!'));
      }
    } catch (e) {
      onFailed(
          RespondModel(BaseClient.DEFAULT_ERROR_CODE, null, message: 'error!'));
    }
  }

  void validateRespond(Response respond, VoidCallback onRetry,
      OnComplete onComplete, OnFailed onFailed, int counter) {
    var isRetry = false;
    if (intercepters != null && intercepters.isNotEmpty) {
      loop:
      for (var intercepter in intercepters) {
        Process status = intercepter.intercep(respond);
        switch (status) {
          case Process.Retry:
            if (counter != null) {
              if (counter >= 1) {
                isRetry = true;
                onRetry();
              } else {
                isRetry = false;
              }
            } else {
              isRetry = false;
            }
            break loop;
          case Process.End:
            break loop;
          case Process.Abort:
            break loop;
          default:
            break;
        }
      }
    }

    if (!isRetry) {
      if (respond.statusCode == BaseClient.SUCCESS_CODE && onComplete != null) {
        onComplete(respond);
        return;
      }

      if (onFailed != null) {
        onFailed(RespondModel(BaseClient.DEFAULT_ERROR_CODE, null,
            message: "error"));
      }
    }
  }
}

class HttpClientBuilder extends ClientBuilder {
  HttpClientBuilder();
  @override
  HttpService build() {
    return HttpClient(this);
  }
}
