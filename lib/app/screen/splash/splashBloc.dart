import 'package:hello_world/app/common/constant/constString.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../core/bloc/base_bloc_component.dart';
import '../../../core/event/observable.dart';

class SplashBloc extends BaseBloc {
  var logoWidthObservable = ObservableField<double>(500);

  @override
  void dispose() {
    logoWidthObservable.close();
    super.dispose();
  }

  Future<String> getCurrentChildId() async {
    var pref = await SharedPreferences.getInstance();
    try {
      var id = pref.getString(ConstString.TOKEN);
      return id;
    } on Exception {
      return '';
    }
  }
}
