import 'dart:async';

import 'package:hello_world/core/core.dart';

import '../../../app/common/theme/styles.dart';
import '../../../core/bloc/bloc_provider.dart';
import '../../../core/ui/common_view/observableWidget.dart';
import '../../../core/ui/screen/base_page.dart';
import 'package:flutter/material.dart';

import 'splashBloc.dart';

class SplashPage extends BasePage<SplashBloc> {
  static final stateKey = GlobalKey<BasePageState>();
  SplashPage({NavigatorState navigator})
      : super(key: stateKey, navigator: navigator);

  AnimationController animationController;
  Tween<double> tween;
  @override
  SplashBloc getBlocData(BuildContext context) {
    return BlocProviders.of<SplashBloc>(context);
  }

  @override
  void onPageInit() {
    animationController = AnimationController(
        vsync: pageKey.currentState,
        duration: Duration(seconds: 3),
        reverseDuration: Duration(seconds: 3));
    tween = Tween<double>(begin: -4, end: 4);
    var animateValue = animationController.drive(tween);

    animationController.addListener(() {
      bloc.logoWidthObservable.set(animateValue.value);
    });

    animationController.repeat(reverse: true);
  }

  @override
  void onInitEvent(BuildContext context) {
    Timer(Duration(seconds: 2), () async {
      var id = await bloc.getCurrentChildId();
      if (id != null && id != '')
        Navigator.pushReplacementNamed(context, '/ChildManagement');
      else
        Navigator.pushReplacementNamed(context, '/Login');
    });
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
          shape: BoxShape.rectangle, gradient: Styles.screenBackgroundGradient),
      position: DecorationPosition.background,
      child: Container(
        margin: EdgeInsets.all(20.0),
        child: ObservableWidget<double>(
          observable: bloc.logoWidthObservable,
          onbuild: (context, data) => Transform(
              transform: Matrix4.rotationY(data),
              alignment: Alignment.center,
              child: Image.asset(
                "assets/images/splash_logo.png",
                alignment: Alignment.center,
                fit: BoxFit.fitWidth,
              )),
        ),
      ),
    );
  }

  @override
  void onDispose() {
    print("${getPageName()} dispose");
    animationController.dispose();
    super.onDispose();
  }
}
