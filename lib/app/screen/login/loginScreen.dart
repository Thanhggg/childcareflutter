import 'package:flutter/material.dart';
import 'package:hello_world/app/common/theme/dimens.dart';
import 'package:hello_world/app/common/view/commonButton.dart';
import 'package:hello_world/app/common/view/commonScreenHolder.dart';
import 'package:hello_world/app/common/view/observableTextField.dart';
import 'package:hello_world/app/common/view/spaceView.dart';
import 'package:hello_world/app/screen/login/loginBloc.dart';
import 'package:hello_world/core/core.dart';

class LoginScreen extends BasePage<LoginBloc> {
  @override
  LoginBloc getBlocData(BuildContext context) {
    return BlocProviders.of<LoginBloc>(context);
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return CommonScreenHolder(
      isShowBackBtn: false,
      child: Container(
        padding: EdgeInsets.only(
            left: Dimension.designDimen(20), right: Dimension.designDimen(20)),
        alignment: Alignment.center,
        child: SingleChildScrollView(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Child Care'.toUpperCase(),
              style: TextStyle(
                  fontFamily: 'Logo', fontSize: 40, color: Colors.white),
              textAlign: TextAlign.center,
            ),
            Space.height(Dimension.designDimen(10)),
            ObservableTextField(
              observableField: bloc.userName,
              hint: 'User name',
            ),
            Space.height(Dimension.designDimen(20)),
            ObservableTextField(
              observableField: bloc.password,
              hint: 'Password',
              isPassword: true,
            ),
            Space.height(Dimension.designDimen(30)),
            CommonButton(
              onBtnPress: () async {
                clearFocus(context);
                bool loginStatus = await bloc.login().catchError((error) {
                  showErrorDialog(context, error.toString());
                });
                if (loginStatus == true) {
                  Navigator.pushReplacementNamed(context, '/ChildManagement');
                }
              },
              title: 'Login',
            )
          ],
        )),
      ),
    );
  }
}
