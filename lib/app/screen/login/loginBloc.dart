import 'dart:convert';

import 'package:hello_world/app/common/constant/constString.dart';
import 'package:hello_world/core/bloc/base_bloc_component.dart';
import 'package:hello_world/core/core.dart';
import 'package:hello_world/core/event/observable.dart';
import 'package:hello_world/core/network2/service/service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc extends BaseBloc {
  ObservableField<String> userName = ObservableField('');
  ObservableField<String> password = ObservableField<String>('');

  Future<bool> login() async {
    var email = userName.value();
    var pass = password.value();
    if (!validate(email, pass)) {
      return false;
    }
    var completer = Completer<bool>();
    showLoading();
    HttpServices.unAuthServices.post(
      '/user/login',
      data: jsonEncode({"email": email, "password": pass}),
      onRespond: (respond) async {
        print('${respond.data}');
        hideLoading();
        if (respond.statusCode == 200) {
          var storage = await SharedPreferences.getInstance();
          await storage.setString(ConstString.TOKEN, respond.data['token']);
          await storage.setString(
              ConstString.REFRESH_TOKEN, respond.data['refreshToken']);
          await storage.setString(ConstString.EMAIL, email);
          completer.complete(true);
        } else
          completer.completeError('Can not login');
      },
      onError: (error) {
        hideLoading();
        completer.completeError('Can not login');
      },
    );
    return completer.future;
  }

  bool validate(String email, pass) {
    if (email.trim() == '') {
      userName.setError(true);
      return false;
    }

    if (pass.trim() == '') {
      password.setError(true);
      return false;
    }

    return true;
  }
}
