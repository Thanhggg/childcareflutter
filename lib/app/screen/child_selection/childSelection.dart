import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hello_world/app/common/model/ChildInfoModel.dart';

import '../../../app/common/theme/dimens.dart';
import '../../../app/common/theme/styles.dart';
import '../../../core/core.dart';
import '../../../core/ui/common_view/observableWidget.dart';
import 'childSelectionBloc.dart';

class ChildSelectionPage extends BasePage<ChildSelectionBloc> {
  @override
  ChildSelectionBloc getBlocData(BuildContext context) {
    return BlocProviders.of<ChildSelectionBloc>(context);
  }

  @override
  void onInitEvent(BuildContext context) {
    super.onInitEvent(context);
    bloc.initChildinfoList();
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(Dimension.pad20,
            MediaQuery.of(context).viewPadding.top, Dimension.pad20, 0.0),
        decoration: BoxDecoration(
            gradient: Styles.screenBackgroundGradient,
            shape: BoxShape.rectangle),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: Dimension.pad37,
            ),
            Text(
              "Select Child",
              style: Theme.of(context).textTheme.headline,
            ),
            SizedBox(
              height: Dimension.pad22,
            ),
            Expanded(
              child: ObservableWidget<List<dynamic>>(
                observable: bloc.childListObserField,
                onbuild: (context, data) => ListView.separated(
                  separatorBuilder: (context, position) =>
                      SizedBox(height: Dimension.pad16),
                  itemCount: data.length,
                  padding: EdgeInsets.only(
                      top: Dimension.pad5, bottom: Dimension.pad5),
                  itemBuilder: (context, position) {
                    var item = data[position];
                    if (item is ChildInfoModel)
                      return buildItem(context, data[position]);
                    else
                      return buildAddnewButton(context);
                  },
                ),
              ),
            )
          ],
        ));
  }

  Widget buildItem(BuildContext context, ChildInfoModel data) {
    return GestureDetector(
      onTap: () async {
        await bloc.saveChildId(data.id);
        Navigator.pushReplacementNamed(context, '/ChildManagement');
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Dimension.pad8),
          gradient: LinearGradient(colors: [
            Colors.accents[2],
            Colors.blue,
          ]),
          shape: BoxShape.rectangle,
        ),
        alignment: Alignment.center,
        padding: EdgeInsets.fromLTRB(
            Dimension.pad20, Dimension.pad10, Dimension.pad20, Dimension.pad10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(Dimension.pad120),
                clipBehavior: Clip.antiAlias,
                child: (data == null || data.avatarPath.trim() == '')
                    ? Image.asset(
                        "assets/images/profile_placeholder.png",
                        alignment: Alignment.center,
                        fit: BoxFit.cover,
                        width: Dimension.pad80,
                        height: Dimension.pad80,
                      )
                    : Image.file(
                        File(data.avatarPath),
                        alignment: Alignment.center,
                        fit: BoxFit.cover,
                        width: Dimension.pad80,
                        height: Dimension.pad80,
                      ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Child: ${data.childName}",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.title,
                    ),
                    SizedBox(
                      height: Dimension.pad10,
                    ),
                    Text(
                      "Date of Birth: ${data.dateOfBirth}",
                      style: Theme.of(context).textTheme.subtitle,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildAddnewButton(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/ChildRegister').then((isAddNewChild) {
          bloc.initChildinfoList();
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.purple[300],
            borderRadius: BorderRadius.circular(Dimension.pad6),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 2.0,
                  blurRadius: Dimension.pad14,
                  color: Colors.black12)
            ]),
        padding: EdgeInsets.fromLTRB(
            Dimension.designDimen(10),
            Dimension.designDimen(20),
            Dimension.designDimen(10),
            Dimension.designDimen(20)),
        child: Center(
          child: Text(
            'Add new Child Profile',
            style: TextStyle(fontSize: 24, color: Colors.black87),
          ),
        ),
      ),
    );
  }

  @override
  void onDispose() {
    print("ChildSelection: dispose");
    super.onDispose();
  }
}
