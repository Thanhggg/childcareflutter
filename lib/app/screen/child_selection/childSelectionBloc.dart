import 'dart:convert';

import 'package:hello_world/app/common/constant/constString.dart';
import 'package:hello_world/app/common/constant/constant.dart';
import 'package:hello_world/app/common/model/ChildInfoModel.dart';
import 'package:hello_world/core/network2/service/service.dart';
import 'package:hello_world/core/storage/database.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../core/bloc/base_bloc_component.dart';
import '../../../core/event/observable.dart';

class ChildSelectionBloc extends BaseBloc {
  List childList;
  ObservableField<List<dynamic>> childListObserField;

  ObservableField<double> moveItem = ObservableField(0);

  ChildSelectionBloc() {
    childListObserField = ObservableField(List());
  }

  @override
  void dispose() {
    childListObserField.close();
    super.dispose();
  }

  void initChildinfoList() async {
    var db = DatabaseProvider.getDatabase();
    List<Map<String, dynamic>> childInfoData = await db.query('childs');
    childList = List();
    if (childInfoData != null && childInfoData.length > 0)
      childList = List.generate(childInfoData.length,
          (index) => ChildInfoModel.fromJson(childInfoData[index]));

    childList.add("add new child");
    childListObserField.set(childList);
  }

  Future<void> saveChildId(int id) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setString(Constant.AVAILABLE_CHILD_ID, id.toString());
  }

  void login(String email, String password,
      Function(bool isLoginSuccess) loginCallback) {
    showLoading();
    HttpServices.unAuthServices.post(
      '/user/login',
      data: jsonEncode({"email": email, "password": password}),
      onRespond: (respond) async {
        if (respond.statusCode == 200) {
          var storage = await SharedPreferences.getInstance();
          await storage.setString(ConstString.TOKEN, 'asd');
          await storage.setString(
              ConstString.REFRESH_TOKEN, respond.data['refreshToken']);
          await storage.setString(ConstString.EMAIL, email);
          loginCallback(true);
        } else
          loginCallback(false);
        print('${respond.data}');
        hideLoading();
      },
      onError: (error) {
        hideLoading();
        print('${error.message}');
        loginCallback(false);
      },
    );
  }

  void getListUser() {
    showLoading();
    HttpServices.authServices.get(
      '/user/listUser',
      onRespond: (respond) {
        hideLoading();
        print(respond.data);
      },
      onError: (error) {
        hideLoading();
        print(error.message);
      },
    );
  }

  void getProduct() {
    HttpServices.authServices.get(
      '/product',
      onRespond: (respond) {},
      onError: (error) {},
    );
  }

  void getUser(String user) {
    showLoading();
    HttpServices.authServices.get(
      '/user/$user',
      onRespond: (respond) {
        hideLoading();
        print(respond.data);
      },
      onError: (error) {
        hideLoading();
        print(error.message);
      },
    );
  }
}
