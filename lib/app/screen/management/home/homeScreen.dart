import 'package:flutter/material.dart';
import 'package:hello_world/app/common/theme/styles.dart';
import 'package:hello_world/app/common/view/commonScreenHolder.dart';
import 'package:hello_world/core/core.dart';

import 'homeBloc.dart';

class HomeScreen extends BasePage<HomeBloc> {
  @override
  getBlocData(BuildContext context) {
    return BlocProviders.of(context);
  }

  @override
  void onInitEvent(BuildContext context) {}

  @override
  Widget onPageBuild(BuildContext context) {
    return CommonScreenHolder(
        isShowBackBtn: false,
        screenDecor: BoxDecoration(
          gradient: Styles.homeBackgroundGradient
        ),
        child: Center(
          child: Text('Home'),
        ));
  }
}
