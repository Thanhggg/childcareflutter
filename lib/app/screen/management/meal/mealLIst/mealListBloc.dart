import 'package:hello_world/core/bloc/base_bloc_component.dart';
import 'package:hello_world/core/event/observable.dart';

class MealListBloc extends BaseBloc {
  ObservableField mealListObs = ObservableField<List<Map<String, dynamic>>>([
    {'time': '12/04/2120', 'type': 'com'},
    {'time': '13/04/2120', 'type': 'mi'},
    {'time': '14/04/2120', 'type': 'chao'},
    {'time': '15/04/2120', 'type': 'bot'}
  ]);
}
