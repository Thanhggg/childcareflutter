import 'package:flutter/material.dart';
import 'package:hello_world/app/common/theme/dimens.dart';
import 'package:hello_world/app/common/theme/styles.dart';
import 'package:hello_world/app/common/view/commonScreenHolder.dart';
import 'package:hello_world/core/core.dart';
import 'package:hello_world/core/ui/common_view/observableWidget.dart';

import 'mealListBloc.dart';

class MealListScreen extends BasePage<MealListBloc> {
  @override
  getBlocData(BuildContext context) {
    return BlocProviders.of(context);
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return CommonScreenHolder(
        isShowBackBtn: false,
        screenTitle: 'Meal List',
        screenDecor: BoxDecoration(gradient: Styles.mealBackgroundGradient),
        trailBtns: <Widget>[
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                onAddBtnClicked(context);
              })
        ],
        child: Container(
          padding: EdgeInsets.all(Dimension.designDimen(20)),
          child: ObservableWidget<List<Map<String, dynamic>>>(
            observable: bloc.mealListObs,
            onbuild: (context, data) => ListView.separated(
                itemBuilder: (context, position) =>
                    Text('${data[position]['type']}'),
                separatorBuilder: (context, position) =>
                    Text('${data[position]['time']}'),
                itemCount: data.length),
          ),
        ));
  }

  void onAddBtnClicked(BuildContext context) {
    Navigator.pushNamed(context, '/addMealScreen');
  }
}
