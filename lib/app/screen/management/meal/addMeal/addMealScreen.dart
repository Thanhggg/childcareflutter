import 'package:flutter/material.dart';
import 'package:hello_world/app/common/theme/styles.dart';
import 'package:hello_world/app/common/view/commonScreenHolder.dart';
import 'package:hello_world/core/core.dart';

import 'addMealBloc.dart';

class AddMealScreen extends BasePage<AddMealBloc> {
  @override
  getBlocData(BuildContext context) {
    return BlocProviders.of(context);
  }


  @override
  Widget onPageBuild(BuildContext context) {
    return CommonScreenHolder(
      screenTitle: 'Add Meal',
      screenDecor: BoxDecoration(gradient: Styles.mealBackgroundGradient),
      child: Center(
        child: Text(
          'Please add Meal',
          style: Theme.of(context).textTheme.display1,
        ),
      ),
    );
  }

  @override
  bool onBackPress(BuildContext context) {
    Navigator.pop(context);
    return true;
  }
}
