import 'package:flutter/material.dart';
import 'package:hello_world/app/common/animation/Animations.dart';
import 'package:hello_world/app/common/theme/styles.dart';
import 'package:hello_world/app/common/view/commonScreenHolder.dart';
import 'package:hello_world/core/core.dart';

import 'addMeal/addMealBloc.dart';
import 'addMeal/addMealScreen.dart';
import 'mealBloc.dart';
import 'mealLIst/mealListBloc.dart';
import 'mealLIst/mealListScreen.dart';

class MealScreen extends BasePage<MealBloc> {
  @override
  getBlocData(BuildContext context) {
    return BlocProviders.of(context);
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return CommonScreenHolder(
      isShowBackBtn: false,
      isShowAppBar: false,
      isScreenHost: true,
      screenDecor: BoxDecoration(gradient: Styles.mealBackgroundGradient),
      child: Navigator(onGenerateRoute: (setting) {
        switch (setting.name) {
          case '/':
            return PageRouteBuilder(
                pageBuilder: (context, _, __) => BlocProvider(
                    bloc: MealListBloc(), widget: MealListScreen()));
            break;
          case '/addMealScreen':
            return Animations.slideTransitionBuilder(
                context: context,
                child:
                    BlocProvider(bloc: AddMealBloc(), widget: AddMealScreen()));
            break;
          default:
            return PageRouteBuilder(
                pageBuilder: (context, _, __) => BlocProvider(
                    bloc: MealListBloc(), widget: MealListScreen()));
            break;
        }
      }),
    );
  }
}
