import 'package:hello_world/app/common/constant/constString.dart';
import 'package:hello_world/core/bloc/base_bloc_component.dart';
import 'package:hello_world/core/event/observable.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChildManagementBloc extends BaseBloc {
  var navigationObs = ObservableField(0);

  Future<void> logOut() async {
    var pref = await SharedPreferences.getInstance();
    await pref.setString(ConstString.TOKEN, '');
    await pref.setString(ConstString.REFRESH_TOKEN, '');
    await pref.setString(ConstString.EMAIL, '');
  }
}
