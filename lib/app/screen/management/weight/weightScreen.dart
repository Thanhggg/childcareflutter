import 'package:flutter/material.dart';
import 'package:hello_world/app/common/theme/styles.dart';
import 'package:hello_world/app/common/view/commonScreenHolder.dart';
import 'package:hello_world/core/core.dart';

import 'weightBloc.dart';

class WeightScreen extends BasePage<WeightBloc> {
  @override
  getBlocData(BuildContext context) {
    return BlocProviders.of(context);
  }


  @override
  Widget onPageBuild(BuildContext context) {
    return CommonScreenHolder(
        isShowBackBtn: false,
        isShowAppBar: false,
        screenDecor: BoxDecoration(gradient: Styles.weightBackgroundGradient),
        child: Column(
          children: <Widget>[
            Expanded(
                child: Center(
              child: Text(bloc.pageName.value()),
            ))
          ],
        ));
  }
}
