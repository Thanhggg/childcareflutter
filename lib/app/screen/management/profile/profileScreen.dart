import 'package:flutter/material.dart';
import 'package:hello_world/app/common/theme/dimens.dart';
import 'package:hello_world/app/common/theme/styles.dart';
import 'package:hello_world/app/common/view/commonScreenHolder.dart';
import 'package:hello_world/core/core.dart';

import 'profileBloc.dart';

class ProfileScreen extends BasePage<ProfileBloc> {
  final String _childName;
  ProfileScreen(this._childName);

  @override
  ProfileBloc getBlocData(BuildContext context) {
    return BlocProviders.of(context);
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return CommonScreenHolder(
        isShowBackBtn: false,
        isShowAppBar: false,
        screenDecor: BoxDecoration(gradient: Styles.profileBackgroundGradient),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: Dimension.designDimen(20),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton.icon(
                  color: Colors.transparent,
                  label: Text(
                    'Logout',
                    style: TextStyle(fontSize: 16),
                  ),
                  icon: Icon(
                    Icons.directions_run,
                    size: Dimension.designDimen(30),
                  ),
                  onPressed: () async {
                    bloc.logout();
                  },
                )
              ],
            ),
            Expanded(
                child: Center(
              child: Text(_childName),
            ))
          ],
        ));
  }
}
