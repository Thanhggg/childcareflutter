import 'package:hello_world/app/common/constant/events.dart';
import 'package:hello_world/core/bloc/base_bloc_component.dart';
import 'package:hello_world/core/core.dart';

class ProfileBloc extends BaseBloc {
  void logout() {
    FlutBus.pushEvent(Events.LOG_OUT, true);
  }
}
