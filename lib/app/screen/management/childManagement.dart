import 'package:flutter/material.dart';
import 'package:hello_world/app/common/constant/events.dart';
import '../../../app/common/theme/styles.dart';
import '../../../core/core.dart';
import '../../../core/ui/common_view/observableWidget.dart';

import 'childManagementBloc.dart';
import 'home/homeBloc.dart';
import 'home/homeScreen.dart';
import 'meal/mealBloc.dart';
import 'meal/mealScreen.dart';
import 'profile/profileBloc.dart';
import 'profile/profileScreen.dart';
import 'weight/weightBloc.dart';
import 'weight/weightScreen.dart';

class ChildManagementPage extends BasePage<ChildManagementBloc> {
  final String _childName;
  static GlobalKey<BasePageState> stateKey = GlobalKey();
  ChildManagementPage(this._childName) : super(key: stateKey);

  AnimationController _controller;

  int lastSelectPos = 0;
  StreamSubscription _logoutSubScription;
  @override
  ChildManagementBloc getBlocData(BuildContext context) {
    return BlocProviders.of<ChildManagementBloc>(context);
  }

  @override
  void onPageInit() {
    _controller = AnimationController(
        vsync: stateKey.currentState,
        duration: Duration(milliseconds: 500),
        value: 0,
        lowerBound: 0,
        upperBound: 1);
    _controller.forward();
    super.onPageInit();
  }

  @override
  void onInitEvent(BuildContext context) {
    super.onInitEvent(context);
    _logoutSubScription =
        FlutBus.listen<bool>(Events.LOG_OUT, (isLogout) async {
      await bloc.logOut();
      Navigator.pushReplacementNamed(context, '/Login');
    });
  }

  @override
  void onDispose() {
    _logoutSubScription?.cancel();
    super.onDispose();
  }

  @override
  Widget onPageBuild(BuildContext context) {
    var mainSubScreen = generateScreen();
    return Container(
        decoration: BoxDecoration(gradient: Styles.screenBackgroundGradient),
        child: ObservableWidget(
          observable: bloc.navigationObs,
          onbuild: (context, position) => Scaffold(
            backgroundColor: Colors.transparent,
            body: IndexedStack(
              alignment: Alignment.center,
              sizing: StackFit.expand,
              children: mainSubScreen
                  .map((item) => (mainSubScreen.indexOf(item) == position)
                      ? FadeTransition(
                          opacity:
                              _controller.drive(Tween(begin: 0.0, end: 1.0)),
                          child: item,
                        )
                      : (mainSubScreen.indexOf(item) == lastSelectPos)
                          ? FadeTransition(
                              opacity: _controller
                                  .drive(Tween(begin: 1.0, end: 0.0)),
                              child: item,
                            )
                          : item)
                  .toList(),
              index: position,
            ),
            bottomNavigationBar: BottomNavigationBar(
              items: [
                BottomNavigationBarItem(
                    icon: Icon(Icons.home), title: Text('Home')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.cake), title: Text('Meal')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.nature), title: Text('Weight')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.people), title: Text('Profile')),
              ],
              unselectedLabelStyle: TextStyle(color: Colors.black),
              unselectedItemColor: Colors.black54,
              selectedItemColor: Colors.purple[400],
              type: BottomNavigationBarType.fixed,
              elevation: 5,
              currentIndex: position,
              onTap: (selectPos) {
                lastSelectPos = position;
                if (lastSelectPos != selectPos) {
                  bloc.navigationObs.set(selectPos);
                  _controller.reset();
                  _controller.forward();
                }
              },
            ),
          ),
        ));
  }

  List<Widget> generateScreen() {
    return [
      BlocProvider(bloc: HomeBloc(), widget: HomeScreen()),
      BlocProvider(bloc: MealBloc(), widget: MealScreen()),
      BlocProvider(bloc: WeightBloc(), widget: WeightScreen()),
      BlocProvider(bloc: ProfileBloc(), widget: ProfileScreen(_childName)),
    ];
  }
}
