import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:hello_world/app/common/constant/ConstString.dart';
import 'package:hello_world/app/common/theme/dimens.dart';
import 'package:hello_world/app/common/view/commonScreenHolder.dart';
import 'package:hello_world/app/common/view/observableTextField.dart';
import 'package:hello_world/core/ui/common_view/observableWidget.dart';
import 'package:hello_world/core/ui/common_view/radio_view.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

import '../../../app/screen/child_registration/childRegisterBloc.dart';
import '../../../core/core.dart';

class ChildRegisterPage extends BasePage<ChildRegisterBloc> {
  ChildRegisterPage({NavigatorState navigator}) : super(navigator: navigator);
  final ScrollController controller = ScrollController();
  final GlobalKey bottomViewKey = GlobalKey();
  final KeyboardVisibilityNotification _keyboardDetector =
      KeyboardVisibilityNotification();
  @override
  ChildRegisterBloc getBlocData(BuildContext context) {
    return BlocProviders.of<ChildRegisterBloc>(context);
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return CommonScreenHolder(
      screenTitle: 'Child Register',
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        controller: controller,
        child: Padding(
          padding: EdgeInsets.fromLTRB(Dimension.designDimen(20), 0,
              Dimension.designDimen(20), Dimension.designDimen(20)),
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              unfocusTextfield(context);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 40,
                ),
                childAvatarView(context),
                SizedBox(
                  height: 30,
                ),
                ObservableTextField(
                    observableField: bloc.childName, hint: 'Child Name'),
                SizedBox(
                  height: Dimension.designDimen(10),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: Dimension.designDimen(5)),
                      child: Text('Gender: '),
                    ),
                    RadioboxView(
                      ['Male', 'Female'],
                      checkPosition: 0,
                      checkBoxController: bloc.gender.getController(),
                    )
                  ],
                ),
                datePickerView(context),
                SizedBox(
                  height: 20,
                ),
                RaisedButton(
                  key: bottomViewKey,
                  onPressed: () async {
                    if (!await bloc.validateInput()) return;
                    if (await bloc.isChildNameExist(bloc.childName.value())) {
                      var isAddNew = await showDialog<bool>(
                          context: context,
                          barrierDismissible: false,
                          builder: (context) => replaceDialog(context));
                      if (isAddNew) {
                        bloc.updateChild();
                        Navigator.pop(context, true);
                      }
                    } else {
                      await bloc.addNewChild();
                      Navigator.pop(context, true);
                    }
                  },
                  child: Text(
                    'Register',
                    style: TextStyle(fontSize: 20),
                  ),
                  color: Colors.purple[200],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool onBackPress(BuildContext context) {
    Navigator.pop(context);
    return true;
  }

  @override
  void onPageInit() {
    _keyboardDetector.addNewListener(onChange: (isShow) {
      if (isShow) {
        RenderBox bottomViewPos =
            bottomViewKey.currentContext.findRenderObject();
        controller.animateTo(bottomViewPos.localToGlobal(Offset.zero).dy,
            duration: Duration(milliseconds: 300), curve: Curves.easeIn);
      }
    });
    super.onPageInit();
  }

  @override
  void onDispose() {
    _keyboardDetector.removeListener(0);
    super.onDispose();
  }

  void onDatePick(BuildContext context, String currentDate) async {
    var datePicked = await showRoundedDatePicker(
        context: context,
        firstDate: DateTime(DateTime.now().year - 50),
        lastDate: DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().day, DateTime.now().hour + 1),
        initialDate: ((currentDate == null) || (currentDate.trim() == ''))
            ? DateTime.now()
            : DateFormat('dd/MM/yyy').parse(currentDate));
    if (datePicked != null) {
      bloc.dateOfBirth.set(DateFormat('dd/MM/yyy').format(datePicked));
    }
  }

  Widget childAvatarView(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 1, color: Colors.black),
            ),
            child: GestureDetector(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(200),
                clipBehavior: Clip.antiAlias,
                child: ObservableWidget(
                  observable: bloc.avatarChooseImg,
                  onbuild: (context, data) {
                    if (data == null)
                      return Image.asset(
                        'assets/images/profile_placeholder.png',
                        width: Dimension.designDimen(140),
                        height: Dimension.designDimen(140),
                      );
                    else
                      return Image.file(
                        File(data),
                        width: Dimension.designDimen(140),
                        height: Dimension.designDimen(140),
                        fit: BoxFit.cover,
                      );
                  },
                  onErrorBuild: (context, error) => Image.asset(
                    'assets/images/profile_placeholder.png',
                    width: Dimension.designDimen(140),
                    height: Dimension.designDimen(140),
                  ),
                ),
              ),
              onTap: () async {
                var image =
                    await ImagePicker.pickImage(source: ImageSource.gallery);
                if (image != null) bloc.avatarChooseImg.set(image.path);
              },
            ),
          ),
          // Container(
          //   padding: EdgeInsets.only(top: 120,left: 90),
          //   child: IconButton(
          //       icon: Icon(
          //         Icons.add_circle,
          //         size: 30,
          //       ),
          //       onPressed: () async {
          //         var image =
          //             await ImagePicker.pickImage(source: ImageSource.gallery);
          //         if (image != null) bloc.avatarChooseImg.set(image.path);
          //       }),
          // ),
        ],
      ),
    );
  }

  Widget datePickerView(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(ConstString.DATE_OF_BIRTH),
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              unfocusTextfield(context);
              onDatePick(context, bloc.dateOfBirth.value());
            },
            child: ObservableWidget(
              observable: bloc.dateOfBirth,
              onbuild: (context, data) => Container(
                  padding: EdgeInsets.only(
                      left: Dimension.designDimen(20),
                      right: Dimension.designDimen(20)),
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: Dimension.designDimen(1))),
                  ),
                  child: Text(
                    '${((data != null) && (data.trim() != '') ? data : ConstString.CHOOSE_A_DATE)}',
                    style: Theme.of(context).textTheme.display1,
                  )),
              onErrorBuild: (context, data) => Container(
                  padding: EdgeInsets.only(
                      left: Dimension.designDimen(20),
                      right: Dimension.designDimen(20)),
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Colors.red[400],
                            style: BorderStyle.solid,
                            width: Dimension.designDimen(1))),
                  ),
                  child: Text(
                    ConstString.CHOOSE_A_DATE,
                    style: TextStyle(color: Colors.red[400]),
                  )),
            ),
          ),
        )
      ],
    );
  }

  void unfocusTextfield(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
  }
}

Widget replaceDialog(BuildContext context, {Function(bool isOk) dialogAction}) {
  return AlertDialog(
    title: Text(ConstString.CHILD_EXISTED),
    contentPadding: EdgeInsets.all(Dimension.designDimen(20)),
    titleTextStyle: Theme.of(context).textTheme.display1,
    actions: <Widget>[
      FlatButton(
        onPressed: () {
          Navigator.pop(context, true);
        },
        child: Text(ConstString.OK),
      ),
      FlatButton(
        onPressed: () {
          Navigator.pop(context, false);
        },
        child: Text(ConstString.CANCEL),
      ),
    ],
  );
}
