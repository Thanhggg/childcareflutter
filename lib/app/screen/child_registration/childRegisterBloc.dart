import 'package:hello_world/core/event/observable.dart';
import 'package:hello_world/core/storage/database.dart';
import 'package:sqflite/sqflite.dart';

import '../../../core/bloc/base_bloc_component.dart';

class ChildRegisterBloc extends BaseBloc {
  ObservableField avatarChooseImg = ObservableField(null);
  ObservableField childName = ObservableField('');
  ObservableField gender = ObservableField<int>();
  ObservableField dateOfBirth = ObservableField('');

  Future<bool> validateInput() async {
    if (childName.value().trim() == '') {
      childName.setError(true);
      return false;
    }

    if (dateOfBirth.value() == '') {
      dateOfBirth.setError(true);
      return false;
    }

    return true;
  }

  Future<bool> isChildNameExist(String childName) async {
    var db = DatabaseProvider.getDatabase();

    List<Map<String, dynamic>> childInfoList = await db.query('childs');

    if (childInfoList == null || childInfoList.isEmpty) return false;

    if (childInfoList.any((item) => item['name'] == childName)) {
      return true;
    }

    return false;
  }

  Future<void> addNewChild() async {
    await DatabaseProvider.getDatabase().insert(
        'childs',
        {
          'name': childName.value(),
          'gender': gender.value(),
          'dateOfBirth': dateOfBirth.value(),
          'imagePath': avatarChooseImg.value() ?? ''
        },
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  void updateChild() async {
    await DatabaseProvider.getDatabase().update(
        'childs',
        {
          'name': childName.value(),
          'gender': gender.value(),
          'dateOfBirth': dateOfBirth.value(),
          'imagePath': avatarChooseImg.value() ?? ''
        },
        where: 'name =?',
        whereArgs: [childName.value()],
        conflictAlgorithm: ConflictAlgorithm.replace);
  }
}
