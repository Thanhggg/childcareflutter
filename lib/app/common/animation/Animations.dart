import 'package:flutter/cupertino.dart';

class Animations {
  Animations._internal();

  static PageRouteBuilder slideTransitionBuilder(
          {@required BuildContext context, @required Widget child}) =>
      PageRouteBuilder(
          pageBuilder: (context, inAnim, outAnim) => child,
          transitionDuration: Duration(milliseconds: 400),
          transitionsBuilder: (context, inAnim, outAnim, child) =>
              SlideTransition(
                child: child,
                position: inAnim.drive(
                    Tween(begin: Offset(1.0, 0.0), end: Offset.zero)
                        .chain(CurveTween(curve: Curves.easeIn))),
              ));

  static PageRouteBuilder presentTransitionBuilder(
          {@required BuildContext context, @required Widget child}) =>
      PageRouteBuilder(
          pageBuilder: (context, inAnim, outAnim) => child,
          transitionDuration: Duration(milliseconds: 400),
          transitionsBuilder: (context, inAnim, outAnim, child) =>
              FadeTransition(
                opacity: inAnim.drive(Tween(begin: 0.0, end: 1.0)
                    .chain(CurveTween(curve: Curves.linear))),
                child: SlideTransition(
                  position: inAnim.drive(
                      Tween(begin: Offset(0.0, 0.2), end: Offset.zero)
                          .chain(CurveTween(curve: Curves.easeIn))),
                  child: FadeTransition(
                    opacity: outAnim.drive(Tween(begin: 1.0, end: 0.0)
                        .chain(CurveTween(curve: Curves.linear))),
                    child: SlideTransition(
                      child: child,
                      position: outAnim.drive(
                          Tween(begin: Offset.zero, end: Offset(0.0, 0.3))
                              .chain(CurveTween(curve: Curves.easeIn))),
                    ),
                  ),
                ),
              ));
}
