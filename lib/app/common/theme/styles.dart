import 'package:flutter/material.dart';

class Styles {
  static const Gradient screenBackgroundGradient = const LinearGradient(
      tileMode: TileMode.clamp,
      colors: [Color(0xFF70e1f5), Color(0xFFffd194)],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);

      static const Gradient homeBackgroundGradient = const LinearGradient(
      tileMode: TileMode.clamp,
      colors: [Color(0xFF4568DC), Color(0xFFB06AB3)],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);

      static const Gradient mealBackgroundGradient = const LinearGradient(
      tileMode: TileMode.clamp,
      colors: [Color(0xFF67B26F), Color(0xFF4ca2cd)],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);

      static const Gradient profileBackgroundGradient = const LinearGradient(
      tileMode: TileMode.clamp,
      colors: [Color(0xFFA770EF), Color(0xFFCF8BF3),Color(0xFFFDB99B)],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);

      static const Gradient weightBackgroundGradient = const LinearGradient(
      tileMode: TileMode.clamp,
      colors: [Color(0xFFffff1c), Color(0xFF00c3ff)],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);
}
