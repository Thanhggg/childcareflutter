import 'package:flutter/cupertino.dart';

class Dimension {
  static int designScreenWidthPx = 0;
  static Size screenSize;
  static double ratio;

  static const double pad10 = 10.0;
  static const double pad20 = 20.0;
  static const double pad37 = 37.0;
  static const double pad22 = 22.0;
  static const double pad16 = 16.0;
  static const double pad5 = 5.0;
  static const double pad8 = 8.0;
  static const double pad14 = 14.0;
  static const double pad120 = 120.0;
  static const double pad6 = 6.0;
  static const double pad80 = 80.0;
  static const double pad100 = 100.0;

  static double screenPercentW(double dimen) {
    try {
      return screenSize.width * dimen / 100;
    } on Exception {
      throw WrongDimenException();
    }
  }

  static double screenPercentH(double dimen) {
    try {
      return screenSize.height * dimen / 100;
    } on Exception {
      throw WrongDimenException();
    }
  }

  static double designDimen(double dimen) {
    try {
      return dimen;
    } on IntegerDivisionByZeroException {
      return 0;
    }
  }

  static init(BuildContext context, {int designWidth = 0}) {
    ratio = MediaQuery.of(context).devicePixelRatio;
    screenSize = Size(MediaQuery.of(context).size.width * ratio,
        MediaQuery.of(context).size.height * ratio);
    designScreenWidthPx = designWidth;
  }
}

class WrongDimenException implements Exception {
  String errMsg() => 'need to init Dimension in App class';
}
