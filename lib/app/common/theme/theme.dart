import 'package:flutter/material.dart';

class AppThemes {
  static var appTheme = ThemeData(
    textTheme: TextTheme(
        headline: TextStyle(
            fontSize: 24.0,
            color: Colors.black,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w400),
        title: TextStyle(
            fontSize: 24.0,
            color: Colors.black,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w400),
        subtitle: TextStyle(
            fontSize: 14.0,
            color: Colors.black,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w400),
        display1: TextStyle(
          fontSize: 16,
          fontStyle: FontStyle.normal,
          color: Colors.black,
        )),
    primarySwatch: Colors.blue,
    pageTransitionsTheme: PageTransitionsTheme(
        builders: {TargetPlatform.android: AppTransitionBuilder()}),
  );
}

class AppTransitionBuilder extends PageTransitionsBuilder {
  @override
  Widget buildTransitions<T>(
      PageRoute<T> route,
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    return SlideTransition(
      position: animation.drive(Tween(begin: Offset(1.0, 0.0), end: Offset.zero)
          .chain(CurveTween(curve: Curves.easeIn))),
      child: child,
    );
  }
}
