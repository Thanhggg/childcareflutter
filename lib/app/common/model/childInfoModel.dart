class ChildInfoModel {
  int id;
  String childName;
  String dateOfBirth;
  int gender;
  String avatarPath;

  ChildInfoModel(this.childName,
      {this.id, this.dateOfBirth, this.gender, this.avatarPath});

  factory ChildInfoModel.fromJson(Map<String, dynamic> json) {
    return ChildInfoModel(json['name'],
        id: json['id'],
        dateOfBirth: json['dateOfBirth'] ?? '',
        gender: json['gender'],
        avatarPath: json['imagePath'] ?? '');
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': childName,
      'dateOfBirth': dateOfBirth,
      'gender': gender,
      'avatarPath': avatarPath
    };
  }
}
