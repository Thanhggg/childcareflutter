import 'package:flutter/material.dart';
import 'package:hello_world/app/common/theme/dimens.dart';

class CommonButton extends StatelessWidget {
  final String title;
  final Function() onBtnPress;
  final TextStyle titleStyle;

  CommonButton(
      {@required this.onBtnPress,
      this.title,
      this.titleStyle = const TextStyle(
        color: Colors.white,
        fontSize: 16,
      )});
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(Dimension.designDimen(8))),
      color: Colors.purple[300],
      padding: EdgeInsets.only(
          top: Dimension.designDimen(10), bottom: Dimension.designDimen(10)),
      onPressed: onBtnPress,
      child: Text(
        title?.toUpperCase() ?? '',
        style: titleStyle,
      ),
    );
  }
}
