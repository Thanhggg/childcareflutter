import 'package:flutter/material.dart';
import 'package:hello_world/app/common/theme/styles.dart';

class CommonScreenHolder extends StatelessWidget {
  final String screenTitle;
  final BoxDecoration screenDecor;
  final Widget child;
  final bool isShowBackBtn;
  final bool isShowAppBar;
  final bool isScreenHost;
  final List<Widget> trailBtns;
  CommonScreenHolder(
      {this.screenTitle = '',
      this.screenDecor =
          const BoxDecoration(gradient: Styles.screenBackgroundGradient),
      @required this.child,
      this.isShowBackBtn = true,
      this.isShowAppBar = true,
      this.isScreenHost = false,
      this.trailBtns});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: screenDecor,
      padding: EdgeInsets.only(
          top: isShowAppBar
              ? 0
              : isScreenHost ? 0 : MediaQuery.of(context).viewPadding.top),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: isShowAppBar
            ? AppBar(
                centerTitle: false,
                title: Text("$screenTitle",
                    style: Theme.of(context).textTheme.headline),
                backgroundColor: Colors.transparent,
                elevation: 0,
                actions: this.trailBtns,
                leading: isShowBackBtn
                    ? BackButton(
                        color: Colors.black,
                      )
                    : null,
              )
            : null,
        body: this.child,
      ),
    );
  }
}
