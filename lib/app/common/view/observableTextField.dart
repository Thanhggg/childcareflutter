import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hello_world/core/event/observable.dart';
import 'package:hello_world/core/ui/common_view/observableWidget.dart';

class ObservableTextField extends StatelessWidget {
  final ObservableField observableField;
  final String hint;
  final bool isPassword;

  ObservableTextField(
      {@required this.observableField,
      this.hint = '',
      this.isPassword = false});

  @override
  Widget build(BuildContext context) {
    return ObservableWidget(
      observable: observableField,
      onbuild: (context, data) => TextField(
        obscureText: isPassword,
        maxLines: 1,
        autofocus: false,
        style: Theme.of(context).textTheme.display1,
        decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.white, width: 1)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.white, width: 1)),
            contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            hintText: '$hint',
            hintStyle: TextStyle(color: Colors.black54)),
        onChanged: (text) {
          observableField.set(text);
        },
      ),
      onErrorBuild: (context, error) => TextField(
        obscureText: isPassword,
        maxLines: 1,
        style: Theme.of(context).textTheme.display1,
        autofocus: false,
        decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.red, width: 1)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.red, width: 1)),
            contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            hintText: '$hint',
            hintStyle: TextStyle(
              fontSize: 16,
              fontStyle: FontStyle.normal,
              color: Colors.red,
            )),
        onChanged: (text) {
          observableField.set(text);
        },
      ),
    );
  }
}
