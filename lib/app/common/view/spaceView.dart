import 'package:flutter/material.dart';

class Space extends StatelessWidget {
  final double width;
  final double height;

  Space({this.width = 0.0, this.height = 0.0});
  factory Space.height(double height) {
    return Space(
      width: 0.0,
      height: height,
    );
  }

  factory Space.width(double width) {
    return Space(
      width: width,
      height: 0.0,
    );
  }
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
    );
  }
}
