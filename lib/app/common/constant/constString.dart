class ConstString {
  static const CHOOSE_A_DATE = 'choose a date';
  static const DATE_OF_BIRTH = 'Date of birth: ';
  static const CHILD_EXISTED =
      'this child is already be registered \ndo you want to replace';

  static const String CANCEL = 'Cancel';
  static const String OK = 'Ok';

  static const String BEARER = 'bearer';

  static const String TOKEN = 'TOKEN';
  static const String REFRESH_TOKEN = 'REFRESH_TOKEN';
  static const String EMAIL = 'EMAIL';
}
