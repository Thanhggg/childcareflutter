import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hello_world/app/common/constant/constString.dart';
import 'package:hello_world/app/screen/login/loginBloc.dart';
import 'package:hello_world/app/screen/login/loginScreen.dart';
import 'package:hello_world/core/network2/service/authService.dart';
import 'package:hello_world/core/network2/service/intercepter/loggingIntercepter.dart';
import 'package:hello_world/core/network2/service/intercepter/tokenIntercepter.dart';
import 'package:hello_world/core/network2/service/nonAuthService.dart';
import 'package:hello_world/core/network2/service/service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app/common/animation/Animations.dart';
import '../app/common/theme/dimens.dart';
import '../app/common/theme/theme.dart';
import '../app/mainBloc.dart';
import '../core/app/app.dart';
import '../core/bloc/bloc.dart';
import '../core/core.dart';
import '../core/storage/database.dart';
import 'screen/child_registration/childRegister.dart';
import 'screen/child_registration/childRegisterBloc.dart';
import 'screen/child_selection/childSelection.dart';
import 'screen/child_selection/childSelectionBloc.dart';
import 'screen/management/childManagement.dart';
import 'screen/management/childManagementBloc.dart';
import 'screen/splash/splash.dart';
import 'screen/splash/splashBloc.dart';

class MyApp extends BaseApp {
  final String title;
  final GlobalKey<NavigatorState> navigator = GlobalKey<NavigatorState>();
  MyApp(this.title) : super(title);

  @override
  Widget rootBuilder(BuildContext context) {
    return BlocProvider(
      bloc: MainBloc(),
      widget: Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Navigator(
          key: navigator,
          onGenerateRoute: (setting) {
            switch (setting.name) {
              case '/':
                return PageRouteBuilder(
                    pageBuilder: (_, __, ___) => BlocProvider(
                        bloc: SplashBloc(),
                        widget: SplashPage(
                          navigator: navigator.currentState,
                        )));
              case '/Login':
                return Animations.presentTransitionBuilder(
                    context: context,
                    child: BlocProvider(
                      bloc: LoginBloc(),
                      widget: LoginScreen(),
                    ));
                break;
              case '/ChildSelection':
                return Animations.slideTransitionBuilder(
                    context: context,
                    child: BlocProvider(
                      bloc: ChildSelectionBloc(),
                      widget: ChildSelectionPage(),
                    ));
              case '/ChildRegister':
                return Animations.slideTransitionBuilder(
                    context: context,
                    child: BlocProvider(
                      bloc: ChildRegisterBloc(),
                      widget:
                          ChildRegisterPage(navigator: navigator.currentState),
                    ));
              case '/ChildManagement':
                return Animations.slideTransitionBuilder(
                    context: context,
                    child: BlocProvider(
                      bloc: ChildManagementBloc(),
                      widget: ChildManagementPage(''),
                    ));
              default:
                return PageRouteBuilder(
                    pageBuilder: (_, __, ___) => BlocProvider(
                          bloc: SplashBloc(),
                          widget: SplashPage(navigator: navigator.currentState),
                        ));
            }
          },
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  @override
  void onInitFeature(BuildContext context) {
    Dimension.init(context, designWidth: 1080);
    FlutBus.init();
    DatabaseProvider.init('childManagement.db', version: 1, onCreate: (db, _) {
      return db.execute(
          "CREATE TABLE childs(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, dateOfBirth TEXT,gender INTEGER,imagePath TEXT)");
    });

    _initHttpService();
  }

  @override
  ThemeData setupTheme() {
    return AppThemes.appTheme;
  }

  @override
  void onAppDispose() {
    print("App dispose");
    FlutBus.unSubscribleAll();
    DatabaseProvider.getDatabase().close();
    HttpServices.cancelRequests();
    super.onAppDispose();
  }

  void _initHttpService() async {
    var tokenStorage = await SharedPreferences.getInstance();
    HttpServices.init(
        authenService: DioAuthService().init(
            interceptors: [
              LoggingInterceptor(),
              TokenIntercepter(
                  dio: DioAuthService(), tokenStorage: tokenStorage)
            ],
            baseOptions: BaseOptions(
                baseUrl: 'https://nestapisd.herokuapp.com',
                receiveTimeout: 30000,
                connectTimeout: 30000,
                sendTimeout: 30000,
                responseType: ResponseType.json),
            authenticationSchema: ConstString.BEARER,
            getTokenMethod: () async {
              var pref = await SharedPreferences.getInstance();
              return pref.getString(ConstString.TOKEN);
            }),
        unAuthenService: DioNonAuthService().init(
          interceptors: [
            LoggingInterceptor(),
          ],
          baseOptions: BaseOptions(
              baseUrl: 'https://nestapisd.herokuapp.com',
              receiveTimeout: 30000,
              connectTimeout: 30000,
              sendTimeout: 30000,
              responseType: ResponseType.json),
        ));
  }
}

void main() => runApp(MyApp("Child Meal Management"));
