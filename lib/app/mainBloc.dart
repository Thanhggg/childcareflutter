import '../core/bloc/base_bloc_component.dart';
import '../core/event/observable.dart';

class MainBloc extends BaseBloc {
  var counter = ObservableField<int>(1);

  @override
  void dispose() {
    counter.close();
    super.dispose();
  }
}
